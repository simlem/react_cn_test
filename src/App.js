import './App.css';
import Header from './components/shared/Header/Header';
import Home from './components/Home/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom/cjs/react-router-dom.min';
import Apartments from './components/Apartments/Apartments';
import ApartmentDetail from './components/ApartmentDetail/ApartmentDetail';
import AddApartment from './components/AddApartment/AddApartment';
import AddRoom from './components/AddRoom/AddRoom';
import Rooms from './components/Rooms/Rooms';
import Clients from './components/Clients/Clients';
import AddClient from './components/AddClient/AddClient';
import ClientDetail from './components/ClientDetail/ClientDetail';
import NavigationBar from './components/shared/NavigationBar/NavigationBar';
import Reservations from './components/Reservations/Reservations';
import ReservationDetail from './components/ReservationDetail/ReservationDetail';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
       <Header></Header>
       <NavigationBar/>

       <Switch>
          <Route path="/" component={Home} exact/>
          <Route path="/apartments" component={Apartments} exact/>         
          <Route path="/apartments/:id" component={ApartmentDetail} exact/>
          <Route path="/add-apartment" component={AddApartment} exact/>
          <Route path="/apartments/:id/add-room" component={AddRoom} exact/>
          <Route path="/rooms" component={Rooms} exact/>
          <Route path="/clients" component={Clients} exact/>
          <Route path="/clients/:id" component={ClientDetail} exact/>
          <Route path="/add-client" component={AddClient} exact/>
          <Route path="/reservations" component={Reservations} exact/>
          <Route path="/reservations/:id" component={ReservationDetail} exact/>

       </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
