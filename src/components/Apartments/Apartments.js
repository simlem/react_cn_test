import React from 'react';
import { handleResponse } from '../../helpers';
import { API_URL } from '../../config';
import ListApartments from './ListApartments/ListApartments';
import Loading from '../shared/Loading/Loading';


class Apartments extends React.Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            apartments: [],
            error: null
        }
    }

    componentDidMount() {
        this.fetchApartments();
    }

    fetchApartments() {
        this.setState({ loading: true });
        fetch(`${API_URL}/apartment`)
            .then(handleResponse)
            .then((data) => {
                this.setState({
                    apartments: data.apartments,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false });
            });
    }


    render() {
        const {loading, error, apartments} = this.state;
        if(loading ) {
            return <div className="loading-container"><Loading /></div>
        }
        if(error) {
            return <div className="error">{error}</div>
        }
        return (
            <div>
                <ListApartments apartments={apartments}/>
            </div>
        )
    }
}

export default Apartments;