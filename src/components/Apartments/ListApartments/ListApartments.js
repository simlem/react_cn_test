import React from 'react';
import './ListApartments.css';
import PropTypes from 'prop-types';
import {useHistory, withRouter} from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

const ListApartments = (props) => {
    const {apartments} = props;
    
    let sortedApartments = [...apartments];
    sortedApartments.sort((a, b) => {
    if (a.name.toUpperCase() < b.name.toUpperCase()) {
      return -1;
    }
    if (a.name.toUpperCase() > b.name.toUpperCase()) {
      return 1;
    }
        return 0;
    });

    const history = useHistory();

    const routeChange = () =>{ 
        let path = `/add-apartment`; 
        history.push(path);
    }

    return (
        
        <div className="table-container">
        
        <div className="h2 text-center">List of Apartments</div>
        <div className="row">
            <div className="col-md-12 pad-10">
                <button onClick={routeChange} type="submit" className="btn btn-primary">
                    Add apartment
                </button>
            </div>
        </div>
        <br/>
        <br/>
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>Number</th>
                <th>Name</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {sortedApartments.map((apartment) => (
                <tr key={apartment.id}>
                    <td>
                        <span>{apartment.number}</span>
                    </td>
                    <td>
                        <span>{apartment.name}</span>
                    </td>
                    <td>
                        <Button onClick={()=> history.push(`/apartments/${apartment.id}`)} type="button" variant="primary">
                            Detail
                        </Button>
                    </td>
                </tr>
            ))}
            </tbody>
        </Table>
        
    </div>)
};

ListApartments.propTypes = {
    apartments: PropTypes.array.isRequired,
};

export default withRouter(ListApartments);
