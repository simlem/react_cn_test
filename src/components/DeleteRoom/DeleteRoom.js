import React from 'react';
import { API_URL } from '../../config';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';

const DeleteRoom = (props) => {
    const {roomId} = props;

    const deleteRoom = () => {
        fetch(`${API_URL}/room/${roomId}`, {
            method: "delete"
        }).then((result) => {
            console.log(result);

            this.notify();

            this.props.history.push('/');


        }).catch((err) => {
            console.log(err);
        });
            
    }
        
    return(
        <Button onClick={deleteRoom} variant="danger">Delete</Button>
    )
    

}

DeleteRoom.propTypes = {
    roomId: PropTypes.array.isRequired,
};

export default withRouter(DeleteRoom);
