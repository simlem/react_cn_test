import React from 'react';
import { handleResponse } from '../../helpers';
import { API_URL } from '../../config';
import Loading from '../shared/Loading/Loading';
import {withRouter} from 'react-router';

class ReservationDetail extends React.Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            reservation: [],
            error: null
        }

    }
    componentDidMount() {
        const id = this.props.match.params.id;
        this.fetchReservation(id);
    }

    fetchReservation(id) {
        this.setState({ loading: true });
        fetch(`${API_URL}/booking/${id}`)
            .then(handleResponse)
            .then((data) => {
                this.setState({
                    reservation: data.booking,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false });
            });
    }


    render() {
        const {loading, error, reservation} = this.state;
        if(loading) {
            return <div className="loading-container"><Loading /></div>
        }
        if(error) {
            return <div className="error">{error}</div>
        }
        return (
            <>
                <div className="h3 text-center"> Client Info</div>
                <br/>
                <div className="h5">Name : {reservation.client.firstName}{' '}{reservation.client.lastName}</div>
                <div className="h5">Phone Number : {reservation.client.phone}</div>
                <br/>
                <div className="h3">Room Info</div> 
                <div className="h5">Room Number : {reservation.room.number}</div>
                <div className="h5">Room Area : {reservation.room.area}</div>
                <div className="h5">Room Price :{reservation.room.number}</div>
                <br/>
                <div className="h3">Apartment : </div> 
        <div className="h5"> {reservation.room.apartment.number}{' '}{reservation.room.apartment.name}</div>
        <div className="h5">Address : {reservation.room.apartment.street}{' '}{reservation.room.apartment.zipCode}</div>
            </>
        )
    }

}

export default withRouter(ReservationDetail);