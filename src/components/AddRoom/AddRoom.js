import React, { Component } from 'react';
import { API_URL } from '../../config';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

class AddRoom extends Component {
    constructor(props) {
        super(props);

        this.state = {
            number: '',
            area: '',
            price: '' 
        };

        this.handleCreateRoom = this.handleCreateRoom.bind(this);
        this.handleChangeNumber = this.handleChangeNumber.bind(this);
        this.handleChangeArea = this.handleChangeArea.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.createRoom = this.createRoom.bind(this);

    }

    createRoom() {
        const id = this.props.match.params.id;
        let data = {
            "number": this.state.number,
            "area": this.state.area,
            "price": this.state.price,
            "apartmentId": id
        };

        fetch(`${API_URL}/room/`, {
            method: 'post',
            headers: {'Accept': 'application/json', 'Content-Type': 'application/json',},
            body: JSON.stringify(data)
        }).then((result) => {
            console.log(result);

            this.notify();

            this.props.history.push('/');


        }).catch((err) => {
            console.log(err);
        });
    }

    handleCreateRoom = (event) => {
        event.preventDefault();
        this.createRoom();
    };

    handleChangeNumber(event) {
        this.setState({number: event.target.value});
    }

    handleChangeArea(event) {
        this.setState({area: event.target.value});
    }

    handleChangePrice(event) {
        this.setState({price: event.target.value});
    }

    render() {
        return (
            <Form onSubmit={this.handleCreateRoom}>
                <div className="h2 text-center">Add Room</div>
                <Form.Row className="justify-content-md-center">
                    <Col xs="4">
                        <Form.Label>Number</Form.Label>
                        <Form.Control value={this.state.number} onChange={this.handleChangeNumber} name="number" id="number" type="number" required/>
                    </Col>
                </Form.Row>
                <Form.Row className="justify-content-md-center">
                    <Col xs="4">
                        <Form.Label>Area</Form.Label>
                        <Form.Control value={this.state.area} onChange={this.handleChangeArea} name="area" id="area" type="number" required/>
                    </Col>
                </Form.Row>
                <Form.Row className="justify-content-md-center">
                    <Col xs="4">
                        <Form.Label>Price</Form.Label>
                        <Form.Control value={this.state.price} onChange={this.handleChangePrice} name="price" id="price" type="number" required/>
                    </Col>
                </Form.Row>
                <br/>
                <Button type="submit" className="mb-2">
                    Submit
                </Button>
            </Form>
        );
    }
}

export default AddRoom;