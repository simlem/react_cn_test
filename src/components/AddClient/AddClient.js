import React from 'react';
import { API_URL } from '../../config';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

class AddClient extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            nationality: '',
            birthDate: ''
        };       
    }
     
    createClient() {

        let data = {
            "firstName": this.state.firstName,
            "lastName": this.state.lastName,
            "email": this.state.email,
            "phone": this.state.phone,
            "nationality": this.state.nationality,
            "birthDate": this.state.birthDate
        };

        fetch(`${API_URL}/client`, {
            method: 'post',
            headers: {'Accept': 'application/json', 'Content-Type': 'application/json',},
            body: JSON.stringify(data)
        }).then((result) => {
            console.log(result);

            this.notify();

            this.props.history.push('/');


        }).catch((err) => {
            console.log(err);
        });
    };

    handleCreateClient = (event) => {
        event.preventDefault();
        this.createClient();
    };

    handleChangeFirstName = (event) => {
        this.setState({firstName: event.target.value});
    };

    handleChangeLastName = (event) => {
        this.setState({lastName: event.target.value});
    };

    handleChangeEmail = (event) => {
        this.setState({email: event.target.value});
    };

    handleChangePhone = (event) => {
        this.setState({phone: event.target.value});
    };

    handleChangeNationality = (event) => {
        this.setState({nationality: event.target.value});
    };

    handleChangeBirthDate = (event) => {
        this.setState({birthDate: event.target.value});
    };

    render() {
        
        return (
            <>
            <Form onSubmit={this.handleCreateClient}>
                <div className="h2 text-center">Add Client</div>
                <Form.Row className="justify-content-md-center">
                    <Col xs="4">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control value={this.state.firstName} onChange={this.handleChangeFirstName} name="firstName" id="firstName" type="text" required/>
                    </Col>
                    <Col xs="4">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control value={this.state.lastName} onChange={this.handleChangeLastName} name="lastName" id="lastName" type="text" required/>
                    </Col>
                </Form.Row>
                <br/>
                <Form.Row className="justify-content-md-center">
                    <Col xs="8">
                        <Form.Label>Email</Form.Label>
                        <Form.Control value={this.state.email} onChange={this.handleChangeEmail} name="email" id="email" type="email" />
                    </Col>
                </Form.Row>
                <br/>
                <Form.Row className="justify-content-md-center">
                    <Col xs="8">
                        <Form.Label>Phone</Form.Label>
                        <Form.Control value={this.state.phone} onChange={this.handleChangePhone} name="phone" id="phone" type="text" />
                    </Col>
                </Form.Row>
                <br/>
                <Form.Row className="justify-content-md-center">
                    <Col xs="4">
                        <Form.Label>Nationality</Form.Label>
                        <Form.Control value={this.state.nationality} onChange={this.handleChangeNationality} name="nationality" id="nationality" type="text" required/>
                    </Col>
                    
                    <Col xs="4">
                        <Form.Label>Birth Date</Form.Label>
                        <Form.Control value={this.state.birthDate} onChange={this.handleChangeBirthDate} name="birthDate" id="birthDate" type="date" min="1900-01-01" max="2020-11-11" required/>
                    </Col>
                </Form.Row>
                <br/>
                <Button type="submit" className="mb-2">
                    Submit
                </Button>
            </Form>
            </>
        )
    }
}

export default AddClient;
