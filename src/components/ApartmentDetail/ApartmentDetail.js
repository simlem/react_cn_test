import React from 'react';
import { handleResponse } from '../../helpers';
import { API_URL } from '../../config';
import Loading from '../shared/Loading/Loading';
import {withRouter} from 'react-router';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

class ApartmentDetail extends React.Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            rooms: [],
            apartment: [],
            error: null
        }

        this.routeChange = this.routeChange.bind(this);
    }

    routeChange() {
        const id = this.props.match.params.id;
        let path = `${id}/add-room`;
        this.props.history.push(path);
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.fetchApartment(id);
        this.fetchRooms(id);
    }

    fetchApartment(id) {
        this.setState({ loading: true });
        fetch(`${API_URL}/apartment`)
            .then(handleResponse)
            .then((data) => {
                const apartments = data.apartments;
                for(let i =0; i < apartments.length; i++) {
                    if (apartments[i].id === id) {
                        this.setState({apartment : apartments[i]});
                    }
                }
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false});
            });
    }

    fetchRooms(id) {
        this.setState({ loading: true });
        fetch(`${API_URL}/apartment/${id}`)
            .then(handleResponse)
            .then((data) => {
                this.setState({
                    rooms: data.test,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false});
            });
    }


    render() {
        const {loading, error, rooms, apartment} = this.state;
        if(loading) {
            return <div className="loading-container"><Loading /></div>
        }
        if(error) {
            return <div className="error">{error}</div>
        }
        return (
            <>
            <div className="h2">Apartment {apartment.name} number {apartment.number}</div>
            <div className="h4">address : {apartment.street}{' '}{apartment.zipCode}</div>
            <div className="h4"> Rooms</div>
            <div className="table-container">
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>Number</th>
                        <th>Area</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    {rooms.map((room) => (
                        <tr key={room.id}>
                            <td>
                                <span>{room.number}</span>
                            </td>
                            <td>
                                <span>{room.area}</span>
                            </td>
                            <td>
                                <span>{room.price}</span>
                            </td>                        
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </div>
            <Button onClick={this.routeChange} type="submit" variant="primary">Add Room</Button>
            </>
        )
    }

}

export default withRouter(ApartmentDetail);