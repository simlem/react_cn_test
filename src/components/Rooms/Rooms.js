import React from 'react';
import { handleResponse } from '../../helpers';
import { API_URL } from '../../config';
import ListRooms from './ListRooms/ListRooms';
import Loading from '../shared/Loading/Loading';


class Rooms extends React.Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            rooms: [],
            error: null
        }
    }

    componentDidMount() {
        this.fetchRooms();
    }

    fetchRooms() {
        this.setState({ loading: true });
        fetch(`${API_URL}/room`)
            .then(handleResponse)
            .then((data) => {
                this.setState({
                    rooms: data.rooms,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false });
            });
    }


    render() {
        const {loading, error, rooms} = this.state;
        if(loading ) {
            return <div className="loading-container"><Loading /></div>
        }
        if(error) {
            return <div className="error">{error}</div>
        }
        return (
            <div>
                <ListRooms rooms={rooms}/>
            </div>
        )
    }
}

export default Rooms;