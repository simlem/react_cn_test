import React from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import DeleteRoom from'../../DeleteRoom/DeleteRoom';

const ListRooms = (props) => {
    const {rooms} = props;
    
    let sortedRooms = [...rooms];

    sortedRooms.sort((a, b) => {
        if (a.apartment.name.toUpperCase() < b.apartment.name.toUpperCase()) {
            return -1;
        } else if (a.apartment.name.toUpperCase() > b.apartment.name.toUpperCase()) {
            return 1;
        }
        return 0;
    });

    return (<div className="table-container">
        <div className="h2 text-center">List of Rooms</div>
        <br/>
        <br/>
        <table className="table table-striped">
            <thead className="Table-head">
            <tr>
                <th>Apartment Number</th>
                <th>Apartment Name</th>
                <th>Number</th>
                <th>Area</th>
                <th>Price</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {sortedRooms.map((room) => (
                <tr key={room.id}>
                    <td>{room.apartment.number}</td>
                    <td>{room.apartment.name}</td>
                    <td>
                        <span>{room.number}</span>
                    </td>
                    <td>
                        <span>{room.area}</span>
                    </td>
                    <td>
                        <span>{room.price}</span>
                    </td>
                    <td>
                        <DeleteRoom roomId = {room.id}/>
                    </td>
                </tr>
            ))}
            </tbody>
        </table>
    </div>)
};

ListRooms.propTypes = {
    rooms: PropTypes.array.isRequired,
};

export default withRouter(ListRooms);
