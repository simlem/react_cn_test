import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import React from 'react';

class NavigationBar extends React.Component {

    render() {
        return(
            <Navbar bg="light" variant="light">
                <Nav className="mr-auto">
                    <Nav.Link href="/apartments">Apartments</Nav.Link>
                    <Nav.Link href="/rooms">Rooms</Nav.Link>
                    <Nav.Link href="/reservations">Reservations</Nav.Link>
                    <Nav.Link href="/clients">Clients</Nav.Link>
                </Nav>
            </Navbar>
        )
    }
}

export default NavigationBar;