import React from 'react';
import PropTypes from 'prop-types';
import {useHistory, withRouter} from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

const ListReservations = (props) => {
    const {reservations} = props;

    const history = useHistory();

    const routeChange = () =>{ 
        let path = `/add-reservation`; 
        history.push(path);
    }

    return (
    <div className="table-container">
        <div className="h2 text-center">List of Reservations</div>
        <br/>
        <Button variant="primary" onClick={routeChange} type="submit" disabled>
            Add Reservation
        </Button>
        <br/>
        <br/>
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Client Name</th>
                    <th>Room Number</th>
                    <th>Apartment Name</th>
                    <th>Apartment Number</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {reservations.map((reservation) => (
                <tr key={reservation.id}>
                    <td>
                        {reservation.client.firstName}{' '}
                        {reservation.client.lastName}
                    </td>
                    <td>
                        <span>{reservation.room.number}</span>
                    </td>
                    <td>
                        <span>{reservation.room.apartment.name}</span>
                    </td>
                    <td>
                        <span>{reservation.room.apartment.number}</span>
                    </td>
                    <td>
                        <Button onClick={()=> history.push(`/reservations/${reservation.id}`)} variant="primary" >
                            Detail
                        </Button>
                    </td>
                </tr>
                ))}
            </tbody>
        </Table>
    </div>
    )
};

ListReservations.propTypes = {
    reservations: PropTypes.array.isRequired,
};

export default withRouter(ListReservations);
