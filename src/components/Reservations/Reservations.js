import React from 'react';
import { handleResponse } from '../../helpers';
import { API_URL } from '../../config';
import ListReservations from './ListReservations/ListReservations';
import Loading from '../shared/Loading/Loading';


class Reservations extends React.Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            reservations: [],
            error: null
        }
    }

    componentDidMount() {
        this.fetchReservations();
    }

    fetchReservations() {
        this.setState({ loading: true });
        fetch(`${API_URL}/booking`)
            .then(handleResponse)
            .then((data) => {
                this.setState({
                    reservations: data.bookings,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false });
            });
    }


    render() {
        const {loading, error, reservations} = this.state;
        if(loading ) {
            return <div className="loading-container"><Loading /></div>
        }
        if(error) {
            return <div className="error">{error}</div>
        }
        return (
            <div>
                <ListReservations reservations={reservations}/>
            </div>
        )
    }
}

export default Reservations;