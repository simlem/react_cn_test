import React from 'react';
import PropTypes from 'prop-types';
import {useHistory, withRouter} from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

const ListClients = (props) => {
    const {clients} = props;

    const history = useHistory();

    const routeChange = () =>{ 
        let path = `/add-client`; 
        history.push(path);
    }

    return (
        <div className="table-container">
        <div className="h2 text-center">List of Clients</div>
        <br/>
        <Button variant="primary" onClick={routeChange} type="submit">
            Add Client
        </Button>
        <br/>
        <br/>
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Nationality</th>
                <th>Birth Date</th>
            </tr>
            </thead>
            <tbody>
            {clients.map((client) => (
                <tr key={client.id}>
                    <td>{client.lastName}</td>
                    <td>{client.firstName}</td>
                    <td>
                        <span>{client.email}</span>
                    </td>
                    <td>
                        <span>{client.phone}</span>
                    </td>
                    <td>
                        <span>{client.nationality}</span>
                    </td>
                    <td>
                        <span>{client.birthDate}</span>
                    </td>
                    <td>
                    <td>
                        <Button onClick={()=> history.push(`/clients/${client.id}`)} type="button" variant="primary">
                            Detail
                        </Button>
                    </td>
                    </td>
                </tr>
            ))}
            </tbody>
        </Table>
    </div>)
};

ListClients.propTypes = {
    clients: PropTypes.array.isRequired,
};

export default withRouter(ListClients);
