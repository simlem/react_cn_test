import React from 'react';
import { handleResponse } from '../../helpers';
import { API_URL } from '../../config';
import ListClients from './ListClients/ListClients';
import Loading from '../shared/Loading/Loading';


class Clients extends React.Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            clients: [],
            error: null
        }
    }

    componentDidMount() {
        this.fetchClients();
    }

    fetchClients() {
        this.setState({ loading: true });
        fetch(`${API_URL}/client`)
            .then(handleResponse)
            .then((data) => {
                this.setState({
                    clients: data.clients,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false });
            });
    }


    render() {
        const {loading, error, clients} = this.state;
        if(loading ) {
            return <div className="loading-container"><Loading /></div>
        }
        if(error) {
            return <div className="error">{error}</div>
        }
        return (
            <div>
                <ListClients clients={clients}/>
            </div>
        )
    }
}

export default Clients;