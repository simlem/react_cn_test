import React from 'react';
import { API_URL } from '../../config';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

class AddApartment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            number: '',
            name: '',
            roomNumber: '',
            area: '',
            price: ''
        };

        this.handleCreateApartment = this.handleCreateApartment.bind(this);
        this.handleChangeNumber = this.handleChangeNumber.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeRoomNumber = this.handleChangeRoomNumber.bind(this);        
        this.handleChangeArea = this.handleChangeArea.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);        
    }
     
    createApartment() {

        let data = {
            "number": this.state.number,
            "name": this.state.name,
            "rooms": [
                {
                    "number" : this.state.roomNumber,
                    "area" : this.state.area,
                    "price" : this.state.price,
                }
            ]
        };

        fetch(`${API_URL}/apartment`, {
            method: 'post',
            headers: {'Accept': 'application/json', 'Content-Type': 'application/json',},
            body: JSON.stringify(data)
        }).then((result) => {
            console.log(result);

            this.notify();

            this.props.history.push('/');


        }).catch((err) => {
            console.log(err);
        });
    }

    handleCreateApartment = (event) => {
        event.preventDefault();
        this.createApartment();
    };

    handleChangeNumber(event) {
        this.setState({number: event.target.value});
    }

    handleChangeName(event) {
        this.setState({name: event.target.value});
    }

    handleChangeRoomNumber(event) {
        this.setState({roomNumber: event.target.value});
    }

    handleChangeArea(event) {
        this.setState({area: event.target.value});
    }
    handleChangePrice(event) {
        this.setState({price: event.target.value});
    }

    render() {
        
        return (
            <Form onSubmit={this.handleCreateApartment}>
                <div className="h2 text-center">Add Apartment</div>
                <Form.Row className="justify-content-md-center">
                    <Col xs="4">
                        <Form.Label>Number</Form.Label>
                        <Form.Control value={this.state.number} onChange={this.handleChangeNumber} name="number" id="number" type="number" required/>
                    </Col>
                    <Col xs="4">
                        <Form.Label>Name</Form.Label>
                        <Form.Control value={this.state.name} onChange={this.handleChangeName} name="name" id="name" type="text" required/>
                    </Col>
                </Form.Row>
                <br/>
                <div className="h3 text-center">Room</div>
                <br/>
                <Form.Row className="justify-content-md-center">
                    <Col xs="4">
                        <Form.Label>Room Number</Form.Label>
                        <Form.Control value={this.state.roomNumber} onChange={this.handleChangeRoomNumber} name="roomNumber" id="roomNumber" type="number" required/>
                    </Col>
                    <Col xs="4">
                        <Form.Label>Area</Form.Label>
                        <Form.Control value={this.state.area} onChange={this.handleChangeArea} name="area" id="area" type="number" required/>
                    </Col>
                    
                    <Col xs="4">
                        <Form.Label>Price</Form.Label>
                        <Form.Control value={this.state.price} onChange={this.handleChangePrice} name="price" id="price" type="number" required/>
                    </Col>
                </Form.Row>
                <br/>
                <Button type="submit" className="mb-2">
                    Save Apartment
                </Button>
            </Form>
        )
    }
}

export default AddApartment;
