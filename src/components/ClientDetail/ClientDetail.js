import React from 'react';
import { handleResponse } from '../../helpers';
import { API_URL } from '../../config';
import Loading from '../shared/Loading/Loading';
import {withRouter} from 'react-router';

class ClientDetail extends React.Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            client: [],
            error: null
        }

    }
    componentDidMount() {
        const id = this.props.match.params.id;
        this.fetchClient(id);
    }

    fetchClient(id) {
        this.setState({ loading: true });
        fetch(`${API_URL}/client/${id}`)
            .then(handleResponse)
            .then((data) => {
                this.setState({
                    client: data.client,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({ error : error.errorMessage, loading: false });
            });
    }


    render() {
        const {loading, error, client} = this.state;
        if(loading) {
            return <div className="loading-container"><Loading /></div>
        }
        if(error) {
            return <div className="error">{error}</div>
        }
        return (
            <>
                <div className="h2 text-center"> {client.firstName}{' '}{client.lastName}</div>
                <br/>
                <div className="h4">Phone : {client.phone}</div>
                <br/>
                <div className="h4">Birth Date : {client.birthDate}</div>
                <br/>
                <div className="h4">Nationality : {client.nationality}</div>
                <br/>
                <div className="h4">Bookings : {client.bookings.room}</div>
            </>
        )
    }

}

export default withRouter(ClientDetail);